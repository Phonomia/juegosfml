#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML\Audio.hpp>

void controles(sf::RectangleShape &obj,bool colison);
bool colision_cuadrado_circulo(sf::FloatRect &A, sf::FloatRect &B);

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
	window.setFramerateLimit(60);

	sf::Texture chibi;
	chibi.loadFromFile("chibi.png");
	sf::Sprite character;
	character.setTexture(chibi);
	
	sf::Music music_mario;
	music_mario.openFromFile("mario.ogg");

	sf::CircleShape shape1(100.f);
	sf::RectangleShape shape2(sf::Vector2f(200, 150));
	sf::RectangleShape shape3(sf::Vector2f(150, 100));
	shape1.setFillColor(sf::Color::Red);
	shape2.setFillColor(sf::Color::Green);
	shape3.setFillColor(sf::Color::Blue);

	music_mario.play();
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			
		}
		shape1.move(1, 0);
		controles(shape3,colision_cuadrado_circulo(shape3.getGlobalBounds(),shape1.getGlobalBounds()));
		window.clear(sf::Color::White);
		window.draw(shape2);
		window.draw(shape1);
		window.draw(shape3);
		window.draw(character);
		window.display();
	}

	return 0;
}
bool colision_cuadrado_circulo(sf::FloatRect &A, sf::FloatRect &B)
{
	if (A.intersects(B))
		return true;
	else
		return false;
}

void controles(sf::RectangleShape &obj, bool colison)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !colison )
	{
		obj.move(2, 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !colison)
	{
		obj.move(-2, 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !colison)
	{
		obj.move(0, -2);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !colison)
	{
		obj.move(0, 2);
	}
}