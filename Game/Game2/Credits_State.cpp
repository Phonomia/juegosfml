#include "Credits_State.h"
#include "Play_State.h"
#include "Engine.h"
#include <iostream>

namespace State
{
	Credits_State::Creditos::Creditos()
	{
	}

	Credits_State::Creditos::~Creditos()
	{
	}

	Credits_State::Credits_State(Application & app)
		: Game_State(app)
	{
	}

	void State::Credits_State::input()
	{
	}

	void State::Credits_State::update(float dt)
	{
	}

	void State::Credits_State::draw()
	{
		std::cout << "credits state" << std::endl;
	}



}
