#ifndef MENU_STATE_H_INCLUDED
#define MENU_STATE_H_INCLUDED
#include "Game_State.h"
#include "SFML\Graphics.hpp"
#include "SFML\Audio.hpp"

namespace State
{
	class Menu_State:public Game_State
	{
	public:
		Menu_State(Application& app);
		void input() override;
		void update(float dt) override;
		void draw() override;
	private:
	};
}

#endif // MENU_STATE_H_INCLUDED