#pragma once
#include <SFML\Graphics.hpp>

namespace ref
{
	int player_vidas = 1;
	int player_width = 20;
	int player_height = 20;
	float char_vel_x = 0.20;
	float char_vel_y = 0.20;

	float enem_vel = 0.10f;
	float enem_vel2 = 50.0f;

	int obs_width = 30;
	int obs_height = 30;

	int enem_width = 40;
	int enem_height = 40;
}