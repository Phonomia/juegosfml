#ifndef CREDITS_STATE_H_INCLUDED
#define CREDITS_STATE_H_INCLUDED
#include "Game_State.h"
#include "SFML\Graphics.hpp"
#include "SFML\Audio.hpp"
#include<queue>


namespace State
{
	class Credits_State:public Game_State
	{
		class Creditos
		{
		public:
			Creditos();
			~Creditos();

		private:

		};

	public:
		Credits_State(Application& app);
		void input() override;
		void update(float dt) override;
		void draw() override;
	private:
		std::queue<Creditos> m_creditos;
	};
}

#endif // CREDITS_STATE_H_INCLUDED