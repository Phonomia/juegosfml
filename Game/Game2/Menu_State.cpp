#include "Menu_State.h"

#include "Engine.h"
#include <iostream>
#include <SFML\Graphics.hpp>
#include "Game_State.h"
#include "Play_State.h"
#include "Application.h"
#include "Credits_State.h"
#include <iostream>

namespace State
{

	sf::Font font;
	sf::Text text1("Jugar 1 jugador", font,30);
	sf::Text text2("Jugar 2 jugadores", font, 30);
	sf::Text text3("SALIR", font, 30);

	sf::Color color1(255,100,100);//rojo clarito
	sf::Color color2(255, 255, 255);//blanco

	int opcion;

	Menu_State::Menu_State(Application & app)
		: Game_State(app)
	{
		opcion = 1;
		
		font.loadFromFile("arial.ttf");
		text1.setPosition(sf::Vector2f(Engine::WIDTH / 2 - text1.getLocalBounds().width / 2, Engine::HEIGHT / 4 - text1.getLocalBounds().height/2));
		text2.setPosition(sf::Vector2f(Engine::WIDTH / 2 - text2.getLocalBounds().width / 2, (Engine::HEIGHT / 4)*2 - text2.getLocalBounds().height / 2));
		text3.setPosition(sf::Vector2f(Engine::WIDTH / 2 - text3.getLocalBounds().width / 2, (Engine::HEIGHT / 4) * 3 - text3.getLocalBounds().height / 2));

	}
	void Menu_State::input()
	{
	}
	void Menu_State::update(float dt)
	{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down) )
			{
				if (opcion <3)
					opcion++;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up))
			{
				if (opcion >1)
					opcion--;
			}


		switch (opcion)
		{
		case 1:
			text1.setFillColor(color1);
			text2.setFillColor(color2);
			text3.setFillColor(color2);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				m_p_application->changeState(std::make_unique<Play_State>(*m_p_application,1));
			}
			break;
		case 2:
			text1.setFillColor(color2);
			text2.setFillColor(color1);
			text3.setFillColor(color2);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				m_p_application->changeState(std::make_unique<Play_State>(*m_p_application, 2));
			}
			break;
		case 3:
			text1.setFillColor(color2);
			text2.setFillColor(color2);
			text3.setFillColor(color1);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				//m_p_application->changeState(std::make_unique<Credits_State>(*m_p_application));
				Engine::closed();
			}
			break;
		default:
			break;
		}
				
		
	}
	void Menu_State::draw()
	{
		Engine::draw(text1);
		Engine::draw(text2);
		Engine::draw(text3);
		std::cout << opcion << std::endl;
		//std::cout << "menu state" << std::endl;
	}
}