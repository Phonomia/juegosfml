#include "Play_State.h"
#include <iostream>
#include "SFML\Graphics.hpp"
#include <time.h>

#include "Engine.h"
#include "Ref.h"

#include "Game_State.h"
#include "Application.h"
#include "Credits_State.h"

namespace State
{
	int c = 0;
	sf::Texture texture1;
	sf::Texture texture2;
	const sf::Texture *ptexture = &texture2;

	sf::Color color3(255, 100, 100);//rojo clarito
	sf::Color color4(100, 100, 255);//azul clarito


	std::vector<sf::RectangleShape> bombas(2);
	std::vector<sf::RectangleShape> enemigos(6);
	bool isover;

	int jugadores = 0;
	std::vector<sf::Sprite> characters(2);
	std::vector<int> vidas(2);

	void set_vidas();
	void setear_characters(int players);
	void cargar_controles();
	void controles(sf::Sprite &obj, sf::Keyboard::Key arriba, sf::Keyboard::Key abajo, sf::Keyboard::Key izquierda, sf::Keyboard::Key derecha);
	void crear_arrayshapes(std::vector<sf::RectangleShape> &shapes, int tipo);
	void colision_character_shapes(std::vector<sf::RectangleShape> &shapes);
	void draw_shapes(std::vector<sf::RectangleShape> &shapes);

	//8 patrones
	std::vector<int> patron_principal{ 1,-1,1,-1,1,1,-1,-1 };
	std::vector<int> patron_secundario{ -1,-1,1,1,1,-1,1,-1 };
	void move_shapes(std::vector<sf::RectangleShape> &shapes);

	Play_State::Play_State(Application & app,int players)
	: Game_State(app)
	{
		jugadores = players;
		srand(time(0));
		texture1.loadFromFile("Character.png");
		texture2.loadFromFile("Character.png");
		setear_characters(players);
		crear_arrayshapes(bombas,1);
		crear_arrayshapes(enemigos, 2);
		isover = false;
	}
	void Play_State::input()
	{
	}
	void Play_State::update(float dt)
	{
		cargar_controles();
		colision_character_shapes(bombas);
		colision_character_shapes(enemigos);
		/*if (isover)
			m_p_application->changeState(std::make_unique<Credits_State>(*m_p_application));*/

	}
	void Play_State::draw()
	{
		for (int i = 0; i < characters.size(); i++)
		{
			Engine::draw(characters[i]);
		}
		draw_shapes(bombas);
		draw_shapes(enemigos);
		move_shapes(enemigos);
		//std::cout << "playing state"<<std::endl;
		std::cout << jugadores << std::endl;
		

	}

	void set_vidas()
	{
		for (int i = 0; i < vidas.size(); i++)
		{
			vidas[i] = ref::player_vidas;
		}
	}
	void setear_characters(int players)
	{

		if (characters.size() != players)
		{
			characters.resize(players);
			vidas.resize(players);
		}
		for (int i = 0; i < characters.size(); i++)
		{
			switch (i + 1)
			{
			case 1:
				characters[i].setTexture(texture1);
				characters[i].setPosition(((Engine::WIDTH/6)*3)-(characters[i].getLocalBounds().width/2),Engine::HEIGHT-characters[i].getLocalBounds().height);
				characters[i].setOrigin(characters[i].getTextureRect().width / 2, characters[i].getTextureRect().height / 2);
				characters[i].rotate(180);
				characters[i].setColor(color4);
				//characters[i].rotate(180);
				//characters[i].setScale(characters[i].getLocalBounds().width/ref::player_width  ,   characters[i].getLocalBounds().height / ref::player_height);
				break;
			case 2:
				characters[i].setTexture(texture1);
				characters[i].setColor(color3);
				characters[i].setPosition(((Engine::WIDTH / 6) * 4) - (characters[i].getLocalBounds().width / 2), Engine::HEIGHT - characters[i].getLocalBounds().height);
				//characters[i].rotate(180);
				break;
			default:
				break;
			}
		}
	}
	void colision_character_shapes(std::vector<sf::RectangleShape> &shapes)
	{
		for (int i = 0; i < shapes.size(); i++)
		{
			for (int c = 0; c < characters.size(); c++)
			{
				if (characters[c].getGlobalBounds().intersects(shapes[i].getGlobalBounds()) == true)
					vidas[c] = vidas[c] - 1;
			}
		}
	}

	void move_shapes(std::vector<sf::RectangleShape> &shapes)
	{
		int flag = 1;
		for (int i = 0; i < shapes.size(); i++)
		{
			if (i % 4 ==0 && i != 0)
				flag = -flag;
			if(flag ==1)
			{
				if (shapes[i].getPosition().x + shapes[i].getLocalBounds().width > Engine::WIDTH || shapes[i].getPosition().x < 0)
				{
					patron_principal[i%patron_principal.size()] = patron_principal[i%patron_principal.size()] * -1;
					if (shapes[i].getPosition().y + shapes[i].getLocalBounds().height > Engine::HEIGHT || shapes[i].getPosition().y < 0)
					{
						patron_secundario[i%patron_secundario.size()] = patron_secundario[i%patron_secundario.size()] * -1;
					}
					shapes[i].move(0, patron_secundario[i%patron_secundario.size()] * ref::enem_vel2);
				}
				shapes[i].move(patron_principal[i%patron_principal.size()]*ref::enem_vel,0);
			}
			else
			{
				if (shapes[i].getPosition().y + shapes[i].getLocalBounds().height > Engine::HEIGHT || shapes[i].getPosition().y < 0)
				{
					patron_principal[i%patron_principal.size()] = patron_principal[i%patron_principal.size()] * -1;
					if (shapes[i].getPosition().x + shapes[i].getLocalBounds().width > Engine::WIDTH || shapes[i].getPosition().x < 0)
					{
						patron_secundario[i%patron_secundario.size()] = patron_secundario[i%patron_secundario.size()] * -1;
					}
					shapes[i].move(patron_secundario[i] * ref::enem_vel2,0);
				}
				shapes[i].move(0, patron_principal[i%patron_principal.size()]*ref::enem_vel);
			}
		}
	}
	void draw_shapes(std::vector<sf::RectangleShape> &shapes)
	{
		for (int i = 0; i < shapes.size(); i++)
		{
			Engine::draw(shapes[i]);
		}
	}
	void crear_arrayshapes(std::vector<sf::RectangleShape> &shapes, int tipo)
	{
		int width, height,cont;
		switch (tipo)
		{
		case 1:
			width = ref::obs_width;
			height = ref::obs_height;
			break;
		case 2:
			width = ref::enem_width;
			height = ref::enem_height;
			break;
		default:
			break;
		}
		for (int i = 0; i < shapes.size(); i++)
		{
			shapes[i].setSize(sf::Vector2f(width,height));
			do
			{
				cont = 0;
				shapes[i].setPosition(sf::Vector2f(rand() % (Engine::WIDTH - width), rand() % (Engine::HEIGHT - height)));
				if (tipo == 2)
					shapes[i].setTexture(ptexture, false);

				for (size_t i = 0; i < characters.size(); i++)
				{
					if (characters[i].getGlobalBounds().intersects(shapes[i].getGlobalBounds()))
					{
						cont++;
					}
				}
			} while (cont > 0);
		}
	}
	void cargar_controles()
	{
		for (int i = 0; i < characters.size(); i++)
		{
			switch (i+1)
			{
			case 1:
				controles(characters[i], sf::Keyboard::Up, sf::Keyboard::Down, sf::Keyboard::Left, sf::Keyboard::Right);
				break;
			case 2:
				controles(characters[i], sf::Keyboard::W, sf::Keyboard::S, sf::Keyboard::A, sf::Keyboard::D);
				break;
			default:
				break;
			}
		}
	}
	void controles(sf::Sprite &obj, sf::Keyboard::Key arriba, sf::Keyboard::Key abajo, sf::Keyboard::Key izquierda, sf::Keyboard::Key derecha)
	{
		sf::Vector2f centro;
		centro.x = obj.getTextureRect().width / 2;
		centro.y = obj.getTextureRect().height / 2;
		obj.setOrigin(centro);
			if (sf::Keyboard::isKeyPressed(derecha) && (obj.getPosition().x + obj.getLocalBounds().width< Engine::WIDTH))
			{
				if (obj.getRotation() != 180 +90)
				{
					obj.rotate(90);
				}
				
				obj.move(ref::char_vel_x, 0);
			}
			if (sf::Keyboard::isKeyPressed(izquierda) && (obj.getPosition().x >0))
			{
				if (obj.getRotation() != 180 - 90)
				{
					obj.rotate(-90);
				}
				obj.move(ref::char_vel_x*-1, 0);
			}
			if (sf::Keyboard::isKeyPressed(arriba) && (obj.getPosition().y > 0))
			{
				if (obj.getRotation() != 180)
				{
					obj.rotate(90);
				}
				obj.move(0, ref::char_vel_y*-1);
			}
			if (sf::Keyboard::isKeyPressed(abajo) && (obj.getPosition().y + obj.getLocalBounds().height< Engine::HEIGHT))
			{
				if (obj.getRotation() != 0 )
				{
					obj.rotate(-90);
				}
				obj.move(0, ref::char_vel_y);
			}
	}
}