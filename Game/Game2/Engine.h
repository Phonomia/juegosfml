#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

#include <SFML\Graphics.hpp>

namespace Engine
{
	void init();
	void clear();
	void display();

	void draw(const sf::Drawable& drawable);

	void checkWindowEvents();
	bool isOpen();

	void closed();
	constexpr int HEIGHT = 600;
	constexpr int WIDTH = 800;
}
#endif // DISPLAY_H_INCLUDED
