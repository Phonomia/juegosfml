#ifndef PLAY_STATE_H_INCLUDED
#define PLAY_STATE_H_INCLUDED
#include "Game_State.h"

namespace State
{
	class Play_State:public Game_State
	{
	public:
		Play_State(Application& app, int players);
		void input() override;
		void update(float dt) override;
		void draw() override;
	private:
	};
}


#endif // PLAY_STATE_H_INCLUDED