#include "Application.h"
#include "Engine.h"
#include "Play_State.h"
#include "Menu_State.h"

Application::Application()
{
	Engine::init();
	pushState(std::make_unique<State::Menu_State>(*this));
}

Application::~Application()
{
}

void Application::runMainLoop()
{
	while (Engine::isOpen())
	{
		Engine::checkWindowEvents();
		Engine::clear();

		m_states.top()->input();
		m_states.top()->update(0.0f);
		m_states.top()->draw();

		Engine::display();
	}
}

void Application::pushState(std::unique_ptr<State::Game_State> state)
{
	m_states.push(std::move(state));
}

void Application::popState()
{
	m_states.pop();
}

void Application::changeState(std::unique_ptr<State::Game_State> state)
{
	popState();
	pushState(std::move(state));
}
